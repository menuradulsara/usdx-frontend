"use client";

import {
  AppBar,
  Box,
  Button,
  CircularProgress,
  Dialog,
  IconButton,
  InputAdornment,
  Stack,
  TextField,
  Toolbar,
  Typography,
} from "@mui/material";
import { ethers } from "ethers";
import { useEffect, useState } from "react";
import ArbitrageProtocol from "../contracts/ArbitrageProtocol.json";
import USDX from "../contracts/USDX.json";
import ASTRIX from "../contracts/ASTRIX.json";
import BASTRIX from "../contracts/BASTRIX.json";
import Oracle from "../contracts/PriceOracle.json";
import { Addresses } from "@/contracts/adresses";
import { Payments, Refresh, SwapHoriz, SwapVert } from "@mui/icons-material";
import RedeemBonds from "./components/RedeemBonds";
import Image from "next/image";

import ASTRIXIMG from "./assets/images/astrix.png";
import BASTRIXIMG from "./assets/images/bastrix.png";
import USDXIMG from "./assets/images/usdx.png";
import MetamaskImg from "./assets/images/metamask.png";

export default function Home() {
  const [acc, setAcc] = useState<string>();
  const [network, setNetwork] = useState<ethers.Network>();

  const [usdxAmount, setUsdxAmount] = useState<number>(0);
  const [astrixAmount, setAstrixAmount] = useState<number>(0);
  const [bastrixAmount, setBastrixAmount] = useState<number>(0);

  const [usdxPrice, setUsdxPrice] = useState<number>(0);
  const [astrixPrice, setAstrixPrice] = useState<number>(0);

  const [astrixBalance, setAstrixBalance] = useState<number>(0);
  const [bAstrixBalance, setBAstrixBalance] = useState<number>(0);
  const [usdxBalance, setUSDXBalance] = useState<number>(0);

  const [redeemOpen, setRedeemOpen] = useState<boolean>(false);
  const [redeemBondsLoading, setRedeemBondsLoading] = useState<boolean>(false);

  const [loading, setLoading] = useState<boolean>(false);

  const connectWallet = async () => {
    if (typeof window === "undefined") {
      return;
    }
    if (window.ethereum) {
      try {
        // Request account access from MetaMask
        const accounts = await window.ethereum.request({
          method: "eth_requestAccounts",
        });

        console.log(accounts[0]);

        setAcc(accounts[0]);

        console.log("Connected Account:", accounts[0]);
        // Do something with the connected account
      } catch (error) {
        console.error("Error connecting to MetaMask:", (error as any).message);
      }
    } else {
      console.error("Please install MetaMask");
    }
  };

  const getCurrentWallet = async () => {
    if (typeof window === "undefined") {
      return;
    }
    if (window.ethereum) {
      try {
        const accounts = await window.ethereum.request({
          method: "eth_accounts",
        });
        if (accounts.length > 0) {
          return accounts[0]; // Return the first connected account
        } else {
          console.error("No connected accounts found");
        }
      } catch (error) {
        console.error("Error checking for connected wallet:", error);
      }
    } else {
      console.error("Please install MetaMask");
    }

    return null;
  };

  useEffect(() => {
    getProvider();
    const updateAccount = async () => {
      const account = await getCurrentWallet();
      setAcc(account);
    };

    updateAccount();
    getPrices();
    getWalletBalances();

    // Add event listeners for MetaMask
    if (window.ethereum) {
      window.ethereum.on("accountsChanged", () => {
        updateAccount();
        getPrices();
        getWalletBalances();
      }); // Update on account change
      window.ethereum.on("chainChanged", () => window.location.reload()); // Reload on network change
    }

    // Cleanup function for removing the listener
    return () => {
      if (window.ethereum) {
        window.ethereum.removeListener("accountsChanged", updateAccount);
        window.ethereum.removeListener("chainChanged", () =>
          window.location.reload()
        );
      }
    };
  }, []);

  function getInjectedProvider() {
    if (typeof window === "undefined") {
      return;
    }
    if (typeof window.ethereum !== "undefined") {
      return new ethers.BrowserProvider(window.ethereum);
    }
    return null;
  }

  async function getProvider() {
    const ethersProvider = getInjectedProvider();
    if (ethersProvider) {
      const _network = await ethersProvider.getNetwork();
      setNetwork(_network);
      console.log("Connected network:", _network);
    }
  }

  const swapUSDXtoASTRIX = async () => {
    try {
      setLoading(true);
      const ethersProvider = getInjectedProvider(); // Get the MetaMask provider
      if (!ethersProvider) {
        return;
      }
      const signer = await ethersProvider.getSigner();

      const usxdContract = new ethers.Contract(Addresses.usdx, USDX, signer);
      const arbitrageProtocolContract = new ethers.Contract(
        Addresses.arbitrageProtocol,
        ArbitrageProtocol,
        signer
      );
      const res = await usxdContract.approve(
        Addresses.arbitrageProtocol,
        ethers.parseEther(usdxAmount.toString())
      );

      await res.wait();

      const tx = await arbitrageProtocolContract.swapUSDXToASTRIX(
        ethers.parseEther(usdxAmount.toString())
      );
      await tx.wait();
      setLoading(false);
    } catch (e) {
      setLoading(false);
      console.error(e);
    }
  };

  const swapASTRIXToUSDX = async () => {
    setLoading(true);
    try {
      const ethersProvider = getInjectedProvider(); // Get the MetaMask provider
      if (!ethersProvider) {
        return;
      }
      const signer = await ethersProvider.getSigner();
      const astrixContract = new ethers.Contract(
        Addresses.astrix,
        ASTRIX,
        signer
      );
      const arbitrageProtocolContract = new ethers.Contract(
        Addresses.arbitrageProtocol,
        ArbitrageProtocol,
        signer
      );
      const res = await astrixContract.approve(
        Addresses.arbitrageProtocol,
        ethers.parseEther(astrixAmount.toString())
      );
      await res.wait();
      const tx = await arbitrageProtocolContract.swapASTRIXToUSDX(
        ethers.parseEther(astrixAmount.toString())
      );
      await tx.wait();
      setLoading(false);
    } catch (e) {
      setLoading(false);
      console.error(e);
    }
  };

  const getUSDXPrice = async () => {
    const ethersProvider = getInjectedProvider(); // Get the MetaMask provider
    if (!ethersProvider) {
      return;
    }
    const signer = await ethersProvider.getSigner();
    const oracleContract = new ethers.Contract(
      Addresses.priceOracle,
      Oracle,
      signer
    );
    const res = await oracleContract.USDX_PRICE_IN_WEI();
    console.log(ethers.formatEther(res));

    setUsdxPrice(Number(ethers.formatEther(res)));
  };
  const getASTRIXPrice = async () => {
    const ethersProvider = getInjectedProvider(); // Get the MetaMask provider
    if (!ethersProvider) {
      return;
    }
    const signer = await ethersProvider.getSigner();
    const oracleContract = new ethers.Contract(
      Addresses.priceOracle,
      Oracle,
      signer
    );
    const res = await oracleContract.ASTRIX_PRICE_IN_WEI();
    console.log(ethers.formatEther(res));
    setAstrixPrice(Number(ethers.formatEther(res)));
  };

  const getPrices = () => {
    getUSDXPrice();
    getASTRIXPrice();
  };

  const getWalletBalances = async () => {
    const ethersProvider = getInjectedProvider(); // Get the MetaMask provider
    if (!ethersProvider) {
      return;
    }
    const signer = await ethersProvider.getSigner();
    const bastrixContract = new ethers.Contract(
      Addresses.bastrix,
      BASTRIX,
      signer
    );
    const astrixContract = new ethers.Contract(
      Addresses.astrix,
      ASTRIX,
      signer
    );
    const usdxContract = new ethers.Contract(Addresses.usdx, USDX, signer);

    const usdxBalance = await usdxContract.balanceOf(signer.address);
    const astrixBalance = await astrixContract.balanceOf(signer.address);
    const bastrixBalance = await bastrixContract.balanceOf(signer.address);

    setUSDXBalance(Number(ethers.formatEther(usdxBalance)));
    setAstrixBalance(Number(ethers.formatEther(astrixBalance)));
    setBAstrixBalance(Number(ethers.formatEther(bastrixBalance)));
  };

  const redeemBonds = async (_amount: number) => {
    const ethersProvider = getInjectedProvider(); // Get the MetaMask provider
    if (!ethersProvider) {
      return;
    }
    setRedeemBondsLoading(true);
    try {
      const signer = await ethersProvider.getSigner();

      const bastrixContract = new ethers.Contract(
        Addresses.bastrix,
        BASTRIX,
        signer
      );
      const arbitrageProtocolContract = new ethers.Contract(
        Addresses.arbitrageProtocol,
        ArbitrageProtocol,
        signer
      );
      const res = await bastrixContract.approve(
        Addresses.arbitrageProtocol,
        ethers.parseEther(_amount.toString())
      );

      await res.wait();

      const tx = await arbitrageProtocolContract.redeemBASTRXToASTRIX(
        ethers.parseEther(_amount.toString())
      );
      await tx.wait();
      setRedeemBondsLoading(false);
    } catch (e) {
      setRedeemBondsLoading(false);
      console.error(e);
    }
  };

  return (
    <main>
      <Box
        style={{
          backgroundColor: "#e8e8e8",
          minHeight: "100vh",
          color: "black",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <AppBar sx={{ bgcolor: "transparent" }} elevation={0}>
          {/* <Toolbar> */}
          <Stack
            direction="row"
            justifyContent={"end"}
            width={1}
            px={4}
            color={"black"}
            pt={2}
          >
            <Box>
              <Button
                variant="outlined"
                onClick={acc ? undefined : connectWallet}
                disabled={acc !== undefined && acc !== null}
                sx={{
                  bgcolor: "#f5f5f5",
                  color: "black",
                  borderColor: "black",
                  border: "black",
                  ":disabled": {
                    color: "black",
                    borderColor: "#707070",
                  },
                  borderRadius: 30,
                }}
              >
                {acc ? (
                  <Stack direction={"row"} alignItems={"center"} spacing={1}>
                    <Image src={MetamaskImg} alt="metamsk" width={20} />
                    <Typography>
                      Connected {acc?.substring(0, 7) + "..."} on{" "}
                      {network?.name}
                    </Typography>
                  </Stack>
                ) : (
                  "Connect Wallet"
                )}
              </Button>
              {acc !== undefined && acc !== null ? (
                <Stack
                  direction={"row"}
                  alignItems={"center"}
                  mt={1}
                  justifyContent={"end"}
                >
                  <Image
                    src={BASTRIXIMG}
                    alt="bAstrix"
                    width={30}
                    height={30}
                  />
                  <Typography mx={1} fontWeight={700}>
                    {bAstrixBalance.toFixed(2)} bASTRIX
                  </Typography>
                  <Button
                    onClick={() => setRedeemOpen(true)}
                    disabled={usdxPrice < 1 || bastrixAmount < 0}
                    variant="contained"
                    sx={{
                      bgcolor: "black",
                      color: "white",
                      borderRadius: 50,
                      ":hover": {
                        bgcolor: "white",
                        color: "black",
                        border: "solid 1.25px black",
                      },
                    }}
                    disableElevation
                  >
                    Redeem
                    <Payments sx={{ ml: 1 }} />
                  </Button>
                </Stack>
              ) : (
                <></>
              )}
            </Box>
          </Stack>
          {/* </Toolbar> */}
        </AppBar>
        <Stack justifyContent={"center"} alignItems={"center"} mb={5}>
          <Typography mt={3} fontSize={12}>
            Current prices
          </Typography>
          <Stack direction="row">
            <Stack alignItems={"center"}>
              <Typography
                fontWeight={700}
                fontSize={25}
              >{`1 USDX = $${usdxPrice.toFixed(4)}`}</Typography>
              <Typography
                fontWeight={700}
                fontSize={25}
              >{`1 ASTRIX = $${astrixPrice.toFixed(4)}`}</Typography>
            </Stack>
            <IconButton onClick={getPrices}>
              <Refresh />
            </IconButton>
          </Stack>
        </Stack>
        <Box
          sx={{
            border: "solid 0.1px grey",
            width: "50vw",
            p: 3,
            borderRadius: 10,
          }}
        >
          {usdxPrice < 1 ? (
            <Stack spacing={2} mt={5}>
              <TextField
                type="number"
                onChange={(e) => {
                  setUsdxAmount(Number(e.target.value));
                }}
                label="USDX Amount"
                InputProps={{
                  sx: {
                    borderRadius: 50,
                    fontWeight: 700,
                    // color: "white",
                    // fontWeight: 700,
                    // borderColor: "white",
                    // ":focus": { color: "white" },
                  },
                  startAdornment: (
                    <InputAdornment position="start">
                      <Image src={USDXIMG} alt={"USDX"} width={30} />
                    </InputAdornment>
                  ),
                }}
                sx={{
                  "& label": {
                    color: "black",
                  },
                  "& label.Mui-focused": {
                    color: "black",
                  },
                  "& .MuiInput-underline:after": {
                    borderBottomColor: "black",
                  },
                  "& .MuiOutlinedInput-root": {
                    "& fieldset": {
                      borderColor: "black",
                    },
                    "&:hover fieldset": {
                      borderColor: "black",
                    },
                    "&.Mui-focused fieldset": {
                      borderColor: "black",
                    },
                  },
                }}
              />
              <Stack direction={"row"} justifyContent={"center"}>
                <SwapVert sx={{ height: 40, width: 40 }} />
              </Stack>

              <Stack
                direction={"row"}
                justifyContent={"center"}
                alignItems={"center"}
                spacing={2}
                mb={5}
                pb={5}
              >
                <Image
                  src={usdxPrice < 0.95 ? BASTRIXIMG : ASTRIXIMG}
                  alt={"ASTRIX"}
                  width={50}
                />
                <Stack>
                  <Typography fontSize={24} fontWeight={700}>
                    {(usdxAmount / astrixPrice).toFixed(3)}{" "}
                    {usdxPrice < 0.95 ? "bASTRIX" : "ASTRIX"}
                  </Typography>
                  <Typography color={"grey"}>
                    ${((usdxAmount / astrixPrice) * astrixPrice).toFixed(3)}{" "}
                    {`($${(
                      (usdxAmount / astrixPrice) * astrixPrice -
                      usdxAmount * usdxPrice
                    ).toFixed(3)} profit)`}
                  </Typography>
                </Stack>
              </Stack>
              <Box hidden={usdxPrice >= 0.95}>
                <Typography>
                  ASTRIX BOND Tokens are redeemable when
                  <br /> USDX price is above 1$
                </Typography>
                {/* <Typography>{`Profit $${
                  (usdxAmount / astrixPrice) * astrixPrice -
                  usdxAmount * usdxPrice
                }`}</Typography> */}
              </Box>
              <Button
                variant="contained"
                onClick={loading ? undefined : swapUSDXtoASTRIX}
                disabled={usdxPrice > 1}
                sx={{
                  borderRadius: 10,
                  mt: 3,
                  py: 1.5,
                  mx: 12,
                  px: 5,
                  fontWeight: 700,
                  fontSize: 16,
                  bgcolor: "black",
                  ":hover": {
                    bgcolor: "#2e2e2e",
                    border: "solid 1.25px black",
                  },
                }}
                disableElevation
              >
                Swap USDX to {usdxPrice >= 0.95 ? "ASTRIX" : "bASTRIX"}
                {loading ? (
                  <CircularProgress
                    size={22}
                    sx={{
                      color: "white",
                      ml: 2,
                    }}
                  />
                ) : (
                  <></>
                )}
              </Button>
            </Stack>
          ) : (
            <Stack spacing={2} mt={5}>
              <TextField
                type="number"
                onChange={(e) => {
                  setAstrixAmount(Number(e.target.value));
                }}
                label="ASTRIX Amount"
                InputProps={{
                  sx: {
                    borderRadius: 50,
                    fontWeight: 700,
                    // color: "white",
                    // borderColor: "white",
                    // ":focus": { color: "white" },
                  },
                  startAdornment: (
                    <InputAdornment position="start">
                      <Image src={ASTRIXIMG} alt={"ASTRIX"} width={35} />
                    </InputAdornment>
                  ),
                }}
                sx={{
                  "& label": {
                    color: "black",
                  },
                  "& label.Mui-focused": {
                    color: "black",
                  },
                  "& .MuiInput-underline:after": {
                    borderBottomColor: "black",
                  },
                  "& .MuiOutlinedInput-root": {
                    "& fieldset": {
                      borderColor: "black",
                    },
                    "&:hover fieldset": {
                      borderColor: "black",
                    },
                    "&.Mui-focused fieldset": {
                      borderColor: "black",
                    },
                  },
                }}
              />
              <Stack direction={"row"} justifyContent={"center"}>
                <SwapVert sx={{ height: 40, width: 40 }} />
              </Stack>

              <Stack
                direction={"row"}
                justifyContent={"center"}
                alignItems={"center"}
                spacing={2}
                mb={5}
                pb={5}
              >
                <Image src={USDXIMG} alt={"USDX"} width={50} />
                <Stack>
                  <Typography fontSize={24} fontWeight={700}>
                    {(astrixAmount * astrixPrice).toFixed(3)} USDX
                  </Typography>
                  <Typography color={"grey"}>
                    ${(astrixAmount * astrixPrice * usdxPrice).toFixed(3)} ($
                    {(
                      astrixAmount * astrixPrice * usdxPrice -
                      astrixAmount * astrixPrice
                    ).toFixed(3)}{" "}
                    profit)
                  </Typography>
                </Stack>
              </Stack>
              {/* <Stack direction={"row"} justifyContent={"center"}> */}
              <Button
                variant="contained"
                onClick={loading ? undefined : swapASTRIXToUSDX}
                disabled={usdxPrice < 1}
                sx={{
                  borderRadius: 10,
                  mt: 3,
                  py: 1.5,
                  mx: 12,
                  px: 5,
                  fontWeight: 700,
                  fontSize: 16,
                  bgcolor: "black",
                  ":hover": {
                    bgcolor: "#2e2e2e",
                    border: "solid 1.25px black",
                  },
                }}
                disableElevation
              >
                Swap ASTRIX to USDX
                {loading ? (
                  <CircularProgress
                    size={22}
                    sx={{
                      color: "white",
                      ml: 2,
                    }}
                  />
                ) : (
                  <></>
                )}
              </Button>
            </Stack>
            // </Stack>
          )}
        </Box>
      </Box>
      <Dialog
        open={redeemOpen}
        PaperProps={{
          style: { backgroundColor: "black", borderRadius: 25 },
        }}
      >
        <RedeemBonds
          open={redeemOpen}
          amount={bastrixAmount}
          setBastrixAmount={setBastrixAmount}
          onDone={async () => {
            await redeemBonds(bastrixAmount);
            setRedeemOpen(false);
            getPrices();
            getWalletBalances();
          }}
          loading={redeemBondsLoading}
          onClose={() => setRedeemOpen(false)}
          balance={bAstrixBalance}
        />
      </Dialog>
      <Dialog
        open={acc === undefined || acc === null}
        PaperProps={{
          style: { backgroundColor: "black", borderRadius: 25 },
        }}
      >
        <Stack
          bgcolor={"black"}
          height={"50vh"}
          width="40vw"
          justifyContent={"center"}
          p={3}
        >
          <Typography
            fontSize={32}
            color={"white"}
            fontWeight={700}
            align="center"
            mb={5}
          >
            Please connect a wallet to continue.
          </Typography>
          <Button variant="contained" onClick={acc ? undefined : connectWallet}>
            Connect Wallet
          </Button>
        </Stack>
      </Dialog>
    </main>
  );
}
