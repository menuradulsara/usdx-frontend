"use client";

import { SwapVert } from "@mui/icons-material";
import ASTRIXIMG from "../assets/images/astrix.png";
import USDXIMG from "../assets/images/usdx.png";

import {
  Box,
  Button,
  IconButton,
  InputAdornment,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import Image from "next/image";
import React, { ChangeEvent, useState } from "react";

const SwapScreen = () => {
  const [swapToken, setSwapToken] = useState<"ASTRIX" | "USDX">("ASTRIX");
  const [astrixAmount, setAstrixAmount] = useState(0);
  const [usdxAmount, setUsdxAmount] = useState(0);

  const [usdxPrice, setUsdxPrice] = useState<number>(0.945);
  const [astrixPrice, setAstrixPrice] = useState<number>(4.25);

  const handleChange = (
    e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => {
    if (swapToken === "ASTRIX") {
      setAstrixAmount(Number(e.target.value));
    } else {
      setUsdxAmount(Number(e.target.value));
    }
  };

  const handleSwapTokenChange = () => {
    setSwapToken(swapToken === "ASTRIX" ? "USDX" : "ASTRIX");
  };

  return (
    <Stack
      bgcolor={"white"}
      minHeight={"100vh"}
      color={"black"}
      p={5}
      alignItems={"center"}
      justifyContent={"center"}
    >
      <TextField
        label={swapToken}
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <Image
                src={swapToken === "ASTRIX" ? ASTRIXIMG : USDXIMG}
                alt={swapToken}
                width={30}
              />
            </InputAdornment>
          ),
          sx: { borderRadius: 50 },
        }}
        type="number"
        onChange={handleChange}
        sx={{ mb: 2, minWidth: "30vw" }}
      />
      <IconButton onClick={handleSwapTokenChange}>
        <SwapVert />
      </IconButton>
      <Stack
        direction={"row"}
        justifyContent={"center"}
        alignItems={"center"}
        spacing={2}
        mb={5}
      >
        <Image
          src={swapToken === "USDX" ? ASTRIXIMG : USDXIMG}
          alt={swapToken}
          width={50}
        />
        <Stack>
          <Typography fontSize={24} fontWeight={700}>
            {(swapToken === "ASTRIX"
              ? astrixAmount * astrixPrice
              : usdxAmount / astrixPrice
            ).toFixed(3)}
            {swapToken === "USDX"
              ? usdxPrice < 0.95
                ? " ASTRIX BOND"
                : " ASTRIX"
              : " USDX"}
          </Typography>
          <Typography color={"grey"}>
            $
            {swapToken === "ASTRIX"
              ? astrixAmount * astrixPrice * usdxPrice
              : (usdxAmount / astrixPrice) * astrixPrice}
          </Typography>
        </Stack>
      </Stack>
      {swapToken === "ASTRIX" ? (
        <Box>
          <Typography>{`You will get ${astrixAmount * astrixPrice} USDX ~ $${
            astrixAmount * astrixPrice * usdxPrice
          }`}</Typography>
          <Typography>{`Profit $${
            astrixAmount * astrixPrice * usdxPrice - astrixAmount * astrixPrice
          }`}</Typography>
        </Box>
      ) : (
        <Box>
          <Typography>{`You will get ${usdxAmount / astrixPrice} ASTRIX${
            usdxPrice < 0.95 ? " BOND Tokens" : ""
          } ~ $${(usdxAmount / astrixPrice) * astrixPrice}`}</Typography>
          <Typography>{`Profit $${
            (usdxAmount / astrixPrice) * astrixPrice - usdxAmount * usdxPrice
          }`}</Typography>
        </Box>
      )}
    </Stack>
  );
};

export default SwapScreen;
