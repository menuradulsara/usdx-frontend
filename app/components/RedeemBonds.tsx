import { Close, SwapHoriz } from "@mui/icons-material";
import {
  Box,
  Button,
  CircularProgress,
  IconButton,
  InputAdornment,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import Image from "next/image";

import AstrixImg from "../assets/images/astrix.png";
import bAstrixImg from "../assets/images/bastrix.png";
import USDXImg from "../assets/images/usdx.png";

type Props = {
  amount: number;
  setBastrixAmount: (_amount: number) => void;
  onDone: VoidFunction;
  open: boolean;
  onClose: VoidFunction;
  loading: boolean;
  balance: number;
};

const RedeemBonds = ({
  amount,
  setBastrixAmount,
  onDone,
  open,
  onClose,
  loading,
  balance,
}: Props) => {
  return (
    <Box>
      <Stack direction={"row"} justifyContent={"end"}>
        <IconButton onClick={onClose}>
          <Close sx={{ color: "grey", m: 2 }} />
        </IconButton>
      </Stack>
      <Stack
        width="40vw"
        height={"40vw"}
        //   bgcolor={"white"}
        //   borderRadius={5}
        p={5}
        justifyContent={"center"}
      >
        <Stack
          direction={"row"}
          justifyContent={"center"}
          spacing={2.5}
          alignItems={"center"}
          mb={3}
        >
          <Image src={AstrixImg} width={75} alt="astrix" />
          <SwapHoriz sx={{ color: "white" }} />
          <Image src={bAstrixImg} width={75} alt="astrix" />
        </Stack>
        <Typography
          fontWeight={700}
          mb={5}
          fontSize={25}
          align="center"
          color={"white"}
        >
          Redeem ASTRIX BOND Tokens
        </Typography>
        <Typography fontSize={14} color={"GrayText"} mb={0.5} align="right">
          {balance.toFixed(3)} bASTRIX available
        </Typography>
        <TextField
          type="number"
          value={amount}
          onChange={(e) => setBastrixAmount(Number(e.target.value))}
          label="Tokens"
          InputProps={{
            sx: {
              borderRadius: 10,
              color: "white",
              borderColor: "white",
              ":focus": { color: "white" },
            },
            endAdornment: (
              <InputAdornment position="end">
                <Button
                  sx={{ fontWeight: 700 }}
                  onClick={() => setBastrixAmount(balance)}
                >
                  Max
                </Button>
              </InputAdornment>
            ),
          }}
          sx={{
            "& .MuiOutlinedInput-root": {
              "& fieldset": {
                borderColor: "white",
              },
              "&:hover fieldset": {
                borderColor: "white",
              },
              "&.Mui-focused fieldset": {
                borderColor: "white",
              },
            },
            "& label": {
              color: "white",
            },
            "& label.Mui-focused": {
              color: "white",
            },
          }}
        />
        <Typography color={"grey"} fontSize={14} mt={5} align="center">
          1 ASTRIX = 1 bASTRIX
        </Typography>
        <Button
          variant="contained"
          sx={{
            borderRadius: 10,
            mt: 1,
            py: 2,
            mx: 12,
            bgcolor: "white",
            color: "black",
            ":hover": {
              bgcolor: "grey",
            },
          }}
          onClick={loading ? undefined : onDone}
        >
          <Typography fontWeight={700} sx={{ mr: 1 }}>
            Swap Tokens
          </Typography>
          {loading ? (
            <CircularProgress size={22} sx={{ color: "black" }} />
          ) : (
            <SwapHoriz />
          )}
        </Button>
      </Stack>
    </Box>
  );
};

export default RedeemBonds;
